#include "puzzlemodel.h"

#include <QTextStream>
#include <QDebug>
#include <ctime>

QTextStream cout(stdout);

PuzzleModel::PuzzleModel (int row, int col) :
     m_row(row), m_col(col)
{
     int index = 0;
     for(int i = 0; i < m_row; i++) {
        for(int j = 0; j < m_col; j++) {
             tile_dat item;
             item.first = index;
             //  col = x, row = y
             item.second = QPoint(j, i);
             append(item);
             index++;
         }
     }
     empty_index = emptyTilenum();
     printModel();
}

int PuzzleModel::findIndex (int tilenum )
{
    int index = 0;
    for(tile_dat &item : *this) {
        if(item.first == tilenum)
            return index;
        index++;
    }
    return -1;
}

bool PuzzleModel::canSlide (int index)
{
    if(index < 0 || index > (count() - 1)) {
        cout << "out of bounds" << endl;
        return false;
    }
    // check is neighboring:
    QPoint source = (*this)[index].second;
    QPoint target = (*this)[empty_index].second;
    QPoint diff = target - source;
    qDebug() << diff;

    if(     (diff.x() == 0 && diff.y() == -1) || // bottom
            (diff.x() == 0 && diff.y() == 1)  || // top
            (diff.x() == 1 && diff.y() == 0)  || // left
            (diff.x() == -1 && diff.y() == 0)) { // right
        return true;
    }
    cout << "unable to slide ):" << endl;
    return false;
}

void PuzzleModel::slide (int index )
{
    int temp_num;

    // swap tile_dat with empty slot
    temp_num = (*this)[index].first;
    (*this)[index].first = (*this)[empty_index].first;
    (*this)[empty_index].first = temp_num;
    empty_index = index;
    printModel();
}

QList<int> PuzzleModel::neighborTiles ()
{
    QList<int> container;

    // list all possible indexes
    container << empty_index + m_col
              << empty_index - m_col
              << empty_index + 1
              << empty_index - 1;

    // if not slidable then remove:
    for(int &index : container)
        if(!canSlide(index))
            container.removeOne(index);

    return container;
}

bool PuzzleModel::isArranged ( )
{
    int i = 0;
    for(tile_dat &item : *this)
        if(item.first != i++)
            return false;

    return true;
}

void PuzzleModel::printModel()
{
    cout << "printing model..." << endl;
    for(int i = 0; i < count(); i++) {
        cout << (*this)[i].first << "-(" << (*this)[i].second.x() <<
                ", " << (*this)[i].second.y() << ")\t";
        if((i+1) % m_col == 0)
            cout << endl;
    }
    cout << endl;
}

QList<int> PuzzleModel::shuffle(int iterations)
{
    // set seed as time so output is different with every execution
    srand(time(0));

    QList<int> steps_taken;

    for(int i = 0; i != iterations; i++) {
        QList<int> neighbors = neighborTiles();
        int randIndex = neighbors[rand() % neighbors.count()];
        // prevent sliding back and forth
        if(steps_taken.count() > 2) {
            while(randIndex == steps_taken[steps_taken.count() - 2]) {
                randIndex = neighbors[rand() % neighbors.count()];
            }
        }
        steps_taken.append(randIndex);
        slide(randIndex);
    }

    return steps_taken;
}

QPoint PuzzleModel::getPoint(int index)
{
    if(index == -1)
        index = empty_index;

    return (*this)[index].second;
}
