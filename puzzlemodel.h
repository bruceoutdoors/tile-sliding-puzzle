
#ifndef PUZZLEMODEL_H
#define PUZZLEMODEL_H

#include <QList>
#include <QPoint>
#include <QPair>

typedef QPair<int, QPoint> tile_dat;

class PuzzleModel : public QList<tile_dat>
{
public:
    PuzzleModel (int row, int col );
    int findIndex (int tilenum );
    bool canSlide (int index );
    void slide (int index );
    // returns all possible slidable neighboring tiles
    QList<int> neighborTiles ();
    bool isArranged ( );
    // debugging function
    void printModel();
    // returns indexes used to shuffle
    QList<int> shuffle(int iterations);

    // the empty tile number is the last tile
    int emptyTilenum() { return count() - 1; }
    int emptyIndex() { return empty_index; }
    // convenience function. By default gets empty slot
    QPoint getPoint(int index = -1);

private:
    int empty_index;
    int m_row;
    int m_col;
};

#endif // PUZZLEMODEL_H
