#include "puzzlewindow.h"
#include "puzzlemodel.h"
#include "tile.h"

#include <ctime>

#include <QGridLayout>
#include <QDebug>
#include <QMessageBox>
#include <QPropertyAnimation>
#include <QTimer>

static const int SLIDE_TIME = 100;

PuzzleWindow::PuzzleWindow (QString image_path, int row, int col,
                            int iterations) :
    m_row(row), m_col(col), m_iterations(iterations)
{
    game_model = new PuzzleModel(m_row, m_col);

    pixmap.load(image_path);

    // set sizes
    setMinimumSize(pixmap.width()-2, pixmap.height());
    setMaximumSize(pixmap.width()-2, pixmap.height());
    tile_width = pixmap.width() / m_col;
    tile_height = pixmap.height() / m_row;

    // setup timer
    timer = new QTimer(this);
    timer->setInterval(SLIDE_TIME);
    connect(timer, SIGNAL(timeout()), this, SLOT(randMove()));

    num_moves = 0;

    // append all tiles into a container
    for(tile_dat &item : *game_model) {
        if(item.first == game_model->emptyTilenum()) {
            emptySlot = getCoord(item.second);
            continue;
        }
        QRect rectangle(getRect(item.second));
        QPixmap tilemap = pixmap.copy(rectangle);
        Tile *tile = new Tile(tilemap, item.first, SLIDE_TIME, this);
        connect(tile, SIGNAL(clicked()), this, SLOT(slide()));
        tile->setGeometry(rectangle);
        tile_box.append(tile);
    }

    // initialize isStart as false(strangely it's not false by default...)
    isStart = false;
    shuffleTiles(m_iterations);
}

void PuzzleWindow::shuffleTiles(int iterations)
{
    // make tiles read only
    for(Tile* tile : tile_box)
        tile->setEnabled(false);

    // get sequence to shuffle tiles stuff in steps_taken
    PuzzleModel temp_model(m_row, m_col);
    do {
        steps_taken = temp_model.shuffle(iterations);
    } while(temp_model.isArranged());

    qDebug() << steps_taken;

    current_step = steps_taken.begin();
    timer->start();
}

void PuzzleWindow::randMove()
{
    // if reached end of QList, stop timer.
    if(current_step == steps_taken.end()) {
        timer->stop();
        for(Tile* tile : tile_box)
            tile->setEnabled(true);
        isStart = true;
    } else {
        // move tile by emiting its signal
        qDebug() << *current_step;
        int tilenum = (*game_model)[*current_step].first;
        tile_box[tilenum]->clicked();
        current_step++;
    }
}

void PuzzleWindow::slide()
{
    Tile *tile = (Tile*)sender();
    int index = game_model->findIndex(tile->getNumber());

    if(!game_model->canSlide(index)) return;

    // re-add the tile into empty slot
    game_model->slide(index);

    tile->slide(emptySlot);
    emptySlot = getCoord(game_model->getPoint());
    qDebug() << "empty slot: " << emptySlot;

    if(isStart) {
        qDebug() << "moves made: " << ++num_moves;
        if(game_model->isArranged())
            endGame();
    }
}

void PuzzleWindow::endGame ( )
{
    if (QMessageBox::Yes == QMessageBox::question(this, "Congrats!!",
           QString("You suceeded only after %1 moves. Play again?")
               .arg(QString::number(num_moves)) ,
                    QMessageBox::Yes|QMessageBox::No)) {
        isStart = false;
        num_moves = 0;
        shuffleTiles(m_iterations);
    } else {
         this->close();
    }
}

QRect PuzzleWindow::getRect(QPoint point)
{
    point = getCoord(point);
    return QRect(point, QSize(tile_width, tile_height));
}

QPoint PuzzleWindow::getCoord(QPoint point)
{
    return QPoint(point.x() * tile_width, point.y() * tile_height);
}

PuzzleWindow::~PuzzleWindow()
{
    delete game_model;
}
