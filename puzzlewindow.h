#ifndef PUZZLEWINDOW_H
#define PUZZLEWINDOW_H

#include <QWidget>

class QGridLayout;
class PuzzleModel;
class Tile;
class QTimer;
class QPropertyAnimation;

class PuzzleWindow : public QWidget
{
    Q_OBJECT

public:
    explicit PuzzleWindow (QString image_path, int row, int col ,
                           int iterations);
    ~PuzzleWindow();
    // shuffles tiles:
    void shuffleTiles(int iterations);
    void endGame ( );

public slots:
    void slide ();
    void randMove();

private:
    QRect getRect(QPoint point);
    QPoint getCoord(QPoint point);

    // storage container for all pointers to tiles
    QList<Tile*> tile_box;
    PuzzleModel *game_model;
    QTimer *timer;

    QList<int> steps_taken;
    QList<int>::iterator current_step;
    QPoint emptySlot;
    QPixmap pixmap;

    int m_row;
    int m_col;
    int m_iterations;
    int tile_width;
    int tile_height;
    int num_moves;
    bool isStart;
};

#endif // PUZZLEWINDOW_H
