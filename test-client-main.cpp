#include "puzzlemodel.h"
#include <QTextStream>
#include <QDebug>

int main() {
    QTextStream cout(stdout);

    cout << "*~ Sliding Puzzle Test Client!! ~*" << endl << endl;
    PuzzleModel *model = new PuzzleModel(4, 4);
    qDebug() << model->neighborTiles(15);

//    cout << "test find index: " << endl
//         << model->findIndex(15) << endl
//            << model->findIndex(99) << endl << endl;

//    cout << model->canSlide(11) << endl;

    if(model->canSlide(11)) {
        model->slide(11);
    }

    if(model->canSlide(10)) {
        model->slide(10);
    }

    if(model->canSlide(0)) {
        model->slide(0);
    }

    return 0;
}
