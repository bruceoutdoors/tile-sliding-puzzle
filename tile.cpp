#include "tile.h"

#include <QMouseEvent>
#include <QPropertyAnimation>

 Tile::Tile (QPixmap &pixmap, int number, int duration, QWidget *parent) :
     QLabel(parent), m_Number(number)
{
     setPixmap(pixmap);
     setFrameStyle(QFrame::StyledPanel);
     adjustSize();
     setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

     // setup animation
     animation = new QPropertyAnimation(this, "geometry", this);
     animation->setDuration(duration);
     animation->setEasingCurve(QEasingCurve::OutBounce);
 }

 void Tile::slide(QPoint target)
 {
     QRect source(this->pos(), this->size());
     QRect destination(target, this->size());

     animation->setStartValue(source);
     animation->setEndValue(destination);
     animation->start();
 }

 void Tile::mousePressEvent ( QMouseEvent * event )
 {
     emit clicked();
     event->accept();
 }
