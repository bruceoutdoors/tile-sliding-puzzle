#ifndef TILE_H
#define TILE_H

#include <QLabel>
class QPropertyAnimation;

class Tile : public QLabel
{
    Q_OBJECT

public:
    Tile (QPixmap &pixmap, int number,
          int duration = 50, QWidget *parent = 0);
    int getNumber ( ) { return m_Number; }
    void slide(QPoint target);

signals:
    void clicked();

protected:
    void mousePressEvent ( QMouseEvent* event );

private:
    int m_Number;
    QPropertyAnimation *animation;
};

#endif // TILE_H
